const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));


var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});


// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

// const SELECT_ALL_PRODUCTS = "select * from grocery_list";
const SELECT_PRODUCT_BY_ID = "select * from grocery_list where id = ? limit 1";
const INSERT_PRODUCT = "insert into grocery_list (upc12,brand,name) VALUES(?,?,?)";
const UPDATE_PRODUCT_BY_ID = "update grocery_list set upc12 = ?, brand = ?, name = ? where id = ?";
// const SELECT_COUNT_PRODUCTS = "select count(*) as n from grocery_list";
const SELECT_COUNT_PRODUCTS_BY_UPC12 = "select count(*) as n from grocery_list where upc12 = ? limit 1";
const DELETE_PRODUCT_BY_ID = "delete from grocery_list where id = ?";

// const getAllProducts = mkQuery(SELECT_ALL_PRODUCTS, pool);
const getProductById = mkQuery(SELECT_PRODUCT_BY_ID, pool);
const addProduct = mkQuery(INSERT_PRODUCT, pool);
const updateProductById = mkQuery(UPDATE_PRODUCT_BY_ID, pool);
const deleteProductById = mkQuery(DELETE_PRODUCT_BY_ID, pool);
const checkUPC12Exists = mkQuery(SELECT_COUNT_PRODUCTS_BY_UPC12, pool);

//get all products!
app.get("/products", function (req, res) {
    getAllProducts().then(function (result) {
        res.status(200).json(result);
    }).catch(function (err) {
        // console.log(err);
        handleError(err, res);
    });
})

//search for products based on search params
app.post("/products/search", function (req, res) {
    const search = req.body.search;

    var sql = "select * from grocery_list";
    if (search.type == "") {
        search.type = "name"; //default
    }
    sql += " where " + search.type + " like ?"
    sql += " order by " + search.orderby;
    sql += " " + search.order + " limit ? offset ?";

    var fn = mkQuery(sql, pool);;

    if (search.offset == 0) {

        var sql2 = "select count(*) as n from grocery_list where " + search.type + " like ?";
        var fn2 = mkQuery(sql2, pool);

        fn2(search.term).then(function (result) {
            var n = result[0].n;
            if (n > 0) {
                fn(search.term, search.limit, 0).then(function (result) {
                    var data = {
                        n: n,
                        data: result
                    };
                    res.status(200).json(data);
                }).catch(function (err) {
                    handleError(err, res);
                });
            } else {
                res.status(404).send("No products found");
            }
        });
    } else {
        fn(search.term, search.limit, search.offset).then(function (result) {
            // console.log(result);
            res.status(200).json(result);
        }).catch(function (err) {
            // console.log(err);
            handleError(err, res);
        });
    }
});


//get a particular product based on id
app.get("/product/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    // console.log(id);
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }
    getProductById(id).then(function (result) {
        if (result.length > 0) {
            var product = result[0];
            res.status(200).json(product);
        } else {
            res.status(404).send("Product not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});


//add product
app.post("/product", function (req, res) {

    const product = req.body.product;

    checkUPC12Exists(product.upc12)
        .then(function (result) {
            n = parseInt(result[0].n);
            if (n == 0) {
                addProduct(product.upc12, product.brand, product.name)
                    .then(function (result) {
                        // console.log(result);            
                        res.status(201).send("Product added to database");
                    }).catch(function (err) {
                        handleError(err, res);
                    });
            } else {
                res.status(422).send("UPC12 already exists in database");
            }
        }).catch(function (err) {
            handleError(err, res);
        });


});


//update product
app.put("/product/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    const product = req.body.product;

    updateProductById(product.upc12, product.brand, product.name, product.id)
        .then(function (result) {
            // console.log(result);            
            res.status(200).send("Product details are successfully updated.");
        }).catch(function (err) {
            handleError(err, res);
        });
});

//delete a particular product based on id
app.delete("/product/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    // console.log(id);
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }
    
    deleteProductById(id).then(function (result) {
        // console.log(result);
        res.status(202).send("Product deleted from database");
    }).catch(function (err) {
        handleError(err, res);
    });
});


const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

// app.get("/hello", function (req, res) {
//     res.status(200).send("hello back");
// });


//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app