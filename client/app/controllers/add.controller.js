(function () {
    "use strict";
    angular.module("MyApp").controller("AddProductCtrl", AddProductCtrl);

    AddProductCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function AddProductCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.product = {
            upc12: "",
            brand: "",
            name: ""
        };
        vm.showSuccessMessage = false;
        vm.showFailureMessage = false;
        vm.message = "";

        vm.init = function () {
            vm.showSuccessMessage = false;
            vm.showFailureMessage = false;
        }

        vm.init();

        vm.cancelAdd = function () {
            $state.go("products");
        }

        vm.addProduct = function () {
            vm.showSuccessMessage = false;
            vm.showFailureMessage = false;

            MyAppService.addProduct(vm.product)
                .then(function (result) {
                    console.log(result);
                    vm.message = result.data;
                    vm.showSuccessMessage = true;

                    $timeout(function () {
                        $state.go("products");
                    }, 3000);
                }).catch(function (err) {
                    console.log(err);
                    vm.message = err.data;
                    vm.showFailureMessage = true;
                });
        }

    }

})();