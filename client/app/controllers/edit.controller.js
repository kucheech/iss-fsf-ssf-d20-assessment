(function () {
    "use strict";
    angular.module("MyApp").controller("EditProductCtrl", EditProductCtrl);

    EditProductCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function EditProductCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.product = null;
        vm.id = 0;
        vm.showMessage = false;
        vm.message = "";
        vm.enableDelete = false;

        vm.init = function () {
            // console.log($state.params.id);
            vm.id = $state.params.id;
            vm.showMessage = false;
            vm.enableDelete = false;

            MyAppService.getProductById(vm.id)
                .then(function (result) {
                    // console.log(result);
                    vm.product = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.init();

        vm.cancelEdit = function () {
            $state.go("products");
        }

        vm.saveEdit = function () {
            MyAppService.updateProduct(vm.product)
                .then(function (result) {
                    // console.log(result);
                    vm.message = result.data;
                    vm.showMessage = true;

                    $timeout(function () {
                        $state.go("products");
                    }, 3000);
                }).catch(function (err) {
                    console.log(err)
                });
        }

        vm.deleteProduct = function () {

            MyAppService.deleteProductById(vm.product.id)
                .then(function (result) {
                    console.log(result);
                    vm.message = result;
                    vm.showMessage = true;

                    $timeout(function () {
                        $state.go("products");
                    }, 3000);
                }).catch(function (err) {
                    console.log(err)
                });
        }

    }

})();