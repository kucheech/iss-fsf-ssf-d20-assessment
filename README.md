### server unit testing
1. ```npm install --save-dev babel-cli babel-preset-env jest supertest superagent```
1. change test in package.json to jest
1. ```npm test```

### Kill node window process
http://www.wisdomofjim.com/blog/how-kill-running-nodejs-processes-in-windows
```taskkill /im node.exe /F```

### Assessment 2
refer to pdf

### db
refer to grocery_list.sql

### features implemented
* Search default by Name and default order by Name
* Search by brand or product name
* accepts empty input to indicate search for all
* can order results by UPC, Brand and Name
* can select number of results to be displayed per page, default 20
* Add Product page with validation
* Edit Product page with validation and delete function
* Pagination with prev and next buttons updated accordingly